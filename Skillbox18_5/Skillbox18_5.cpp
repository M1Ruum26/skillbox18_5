﻿#include <iostream>

class Stack
{
private:
    int* arr;
    int size;
public:
    Stack()
    {
        arr = nullptr;
        size = 0;
    }

    void push(int a)
    {
        int* tmp;
        tmp = arr;
        arr = new int[size + 1];
        
        size++;
        for (int i = 0; i < size - 1; i++)
            arr[i] = tmp[i];
        arr[size - 1] = a;
        if (size > 1)
            delete[] tmp;
    }

    int pop()
    {
        size--;
        return arr[size];
    }

    Stack(const Stack& st)
    {
        arr = new int[st.size];
        size = st.size;
        for (int i = 0; i < size; i++)
            arr[i] = st.arr[i];
    }

    void print()
    {
        int* p;
        p = arr;
        std::cout << "Array ";
        for (int i = 0; i < size; i++)
        {
            std::cout << *p << ' ';
            p++;
        }
        std::cout << '\n';
    }
};

int main()
{
    Stack v;
    v.push(5);
    v.push(4);
    v.print();
    v.pop();
    v.print();
    v.push(8);
    v.push(3);
    v.push(7);
    v.push(6);
    v.pop();
    v.pop();
    v.print();

}